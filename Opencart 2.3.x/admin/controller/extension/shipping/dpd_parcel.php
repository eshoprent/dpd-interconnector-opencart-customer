<?php
class ControllerExtensionShippingDpdParcel extends Controller {
	private $error = array();

	public function dpd_terminals() {
        $this->load->model('extension/shipping/dpd_parcel');

        $country = $this->request->post['country_id'];
        $city = $this->request->post['city'];

        $terminals = $this->model_shipping_dpd_parcel->getTerminals($country, $city);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($terminals->rows));
    }

    public function dpd_set_order_terminal() {
        $code = $this->request->post['code'];        
        $order_id = $this->request->post['order_id'];

        $this->load->model('shipping/dpd_parcel');

        $this->model_shipping_dpd_parcel->setOrderTerminal($order_id, $code);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode(['shipping_parcel_id' => $code]));
    }

	public function index() {
		$this->load->language('extension/shipping/dpd_parcel');
		$this->load->model('shipping/dpdlivehandler');

		$this->document->setTitle(preg_replace("/<img[^>]+\>/i", "", $this->language->get('heading_title')));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('dpd_parcel', $this->request->post);

			// Updated terminal list
			$this->model_shipping_dpdlivehandler->updateTerminalList();

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true));
		}

		// Breadcrumbs
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_shipping'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => preg_replace("/<img[^>]+\>/i", "", $this->language->get('heading_title')),
			'href' => $this->url->link('extension/shipping/dpd_parcel', 'token=' . $this->session->data['token'], true)
		);

		// Languages variables
		$languages = [
			'heading_title',
			'text_edit',
			'text_none',
			'text_enabled',
			'text_disabled',
			'entry_price',
			'entry_free_shipping',
			'entry_tax_class',
			'entry_status',
			'entry_sort_order',
			'help_free_shipping',
			'button_save',
			'button_cancel',
			'tab_general',
			'entry_cod_status',
			'entry_rate',
			'help_rate'

		];

		foreach ($languages as $value) {
			$data[$value] = preg_replace("/<img[^>]+\>/i", "", $this->language->get($value));
		};

		// Warning messages
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		// Actions
		$data['action'] = $this->url->link('extension/shipping/dpd_parcel', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true);

		$data['price_calculation_method'] = $this->config->get('dpd_setting_price_calculation_parcels');

		// Fields by geo zones
		$this->load->model('localisation/geo_zone');

		$geo_zones = $this->model_localisation_geo_zone->getGeoZones();
		$data['geo_zones'] = $geo_zones;

		$fields_by_geo_zone = [
			'_rate',
			'_price',
			'_free_shipping_from',
			'_status'
		];

		foreach ($geo_zones as $geo_zone) {
			foreach ($fields_by_geo_zone as $field) {
				if (isset($this->request->post['dpd_parcel_' . $geo_zone['geo_zone_id'] . $field])) {
					$data['dpd_parcel_' . $geo_zone['geo_zone_id'] . $field] = $this->request->post['dpd_parcel_' . $geo_zone['geo_zone_id'] . $field];
				} else {
					$data['dpd_parcel_' . $geo_zone['geo_zone_id'] . $field] = $this->config->get('dpd_parcel_' . $geo_zone['geo_zone_id'] . $field);
				}
			}
		}

		$fields = [
			'dpd_parcel_tax_class_id',
			'dpd_parcel_cod_status',
			'dpd_parcel_status',
			'dpd_parcel_sort_order'
		];

		foreach ($fields as $field) {
			if (isset($this->request->post[$field])) {
				$data[$field] = $this->request->post[$field];
			} else {
				$data[$field] = $this->config->get($field);
			}
		}

		// Load tax Class
		$this->load->model('localisation/tax_class');
		$data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		
		// Lod the rest template data
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/shipping/dpd_parcel', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/shipping/dpd_parcel')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}