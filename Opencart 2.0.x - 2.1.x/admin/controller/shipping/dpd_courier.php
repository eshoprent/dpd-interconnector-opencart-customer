<?php
class ControllerShippingDpdCourier extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('shipping/dpd_courier');

		$this->document->setTitle(preg_replace("/<img[^>]+\>/i", "", $this->language->get('heading_title')));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('dpd_courier', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], true));
		}

		// Breadcrumbs
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_shipping'),
			'href' => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => preg_replace("/<img[^>]+\>/i", "", $this->language->get('heading_title')),
			'href' => $this->url->link('shipping/dpd_courier', 'token=' . $this->session->data['token'], true)
		);

		// Languages variables
		$languages = [
			'heading_title',
			'text_edit',
			'text_none',
			'text_enabled',
			'text_disabled',
			'entry_rate',
			'entry_price',
			'entry_free_shipping',
			'entry_tax_class',
			'entry_cod_status',
			'entry_status',
			'entry_sort_order',
			'help_rate',
			'help_free_shipping',
			'button_save',
			'button_cancel',
			'tab_general'	

		];

		foreach ($languages as $value) {
			$data[$value] = preg_replace("/<img[^>]+\>/i", "", $this->language->get($value));
		};

		// Warning messages
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		// Actions
		$data['action'] = $this->url->link('shipping/dpd_courier', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], true);

		$data['price_calculation_method'] = $this->config->get('dpd_setting_price_calculation');

		// Fields by geo zones
		$this->load->model('localisation/geo_zone');

		$geo_zones = $this->model_localisation_geo_zone->getGeoZones();
		$data['geo_zones'] = $geo_zones;

		$fields_by_geo_zone = [
			'_price',
			'_rate',
			'_free_shipping_from',
			'_status'
		];

		foreach ($geo_zones as $geo_zone) {
			foreach ($fields_by_geo_zone as $field) {
				if (isset($this->request->post['dpd_courier_' . $geo_zone['geo_zone_id'] . $field])) {
					$data['dpd_courier_' . $geo_zone['geo_zone_id'] . $field] = $this->request->post['dpd_courier_' . $geo_zone['geo_zone_id'] . $field];
				} else {
					$data['dpd_courier_' . $geo_zone['geo_zone_id'] . $field] = $this->config->get('dpd_courier_' . $geo_zone['geo_zone_id'] . $field);
				}
			}
		}

		$fields = [
			'dpd_courier_tax_class_id',
			'dpd_courier_cod_status',
			'dpd_courier_status',
			'dpd_courier_sort_order'
		];

		foreach ($fields as $field) {
			if (isset($this->request->post[$field])) {
				$data[$field] = $this->request->post[$field];
			} else {
				$data[$field] = $this->config->get($field);
			}
		}

		// Load tax Class
		$this->load->model('localisation/tax_class');
		$data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		
		// Lod the rest template data
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('shipping/dpd_courier.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/dpd_courier')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}