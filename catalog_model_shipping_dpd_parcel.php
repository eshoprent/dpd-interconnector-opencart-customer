if (isset($address['iso_code_2']) && $address['iso_code_2']) {
	$shipping_country_code = $this->session->data['shipping_address']['iso_code_2'];
	
} else {
	$shipping_country_code = $address['iso_code_2'];
}

if (isset($address['city']) && $address['city']) {
	$shipping_city = $this->session->data['shipping_address']['city'];
	
} else {
	$shipping_city = $address['city'];
}