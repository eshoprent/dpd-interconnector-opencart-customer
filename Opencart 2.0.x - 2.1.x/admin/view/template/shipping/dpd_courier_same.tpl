<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-dpd-courier" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
      </div>
      
      <h1><?php echo $heading_title; ?></h1>
      
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
      <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
    <?php } ?>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-dpd-courier" class="form-horizontal">
          <div class="row">
            <div class="col-sm-2">
              <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                <?php foreach ($geo_zones as $geo_zone) { ?>
                  <li><a href="#tab-geo-zone<?php echo $geo_zone['geo_zone_id']; ?>" data-toggle="tab"><?php echo $geo_zone['name']; ?></a></li>
                <?php } ?>
              </ul>
            </div>

            <div class="col-sm-10">
              <div class="tab-content">
                <div class="tab-pane active" id="tab-general">
                  <?php foreach ($languages as $language) { ?>
                    <div class="tab-pane" id="language<?php echo $language['code']; ?>">
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-title<?php echo $language['code']; ?>"><?php echo $entry_title; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="dpd_courier_same_title[<?php echo $language['code']; ?>]" id="input-title<?php echo $language['code']; ?>" class="form-control" value="<?php echo isset($dpd_courier_same_title[$language['code']]) ? $dpd_courier_same_title[$language['code']] : ''; ?>" />
                        </div>
                      </div>
                    </div>
                  <?php } ?>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-tax-class"><?php echo $entry_tax_class; ?></label>
                    <div class="col-sm-10">
                      <select name="dpd_courier_tax_class_id" id="input-tax-class" class="form-control">
                        <option value="0"><?php echo $text_none; ?></option>
                        <?php foreach ($tax_classes as $tax_class) { ?>
                        <?php if ($tax_class['tax_class_id'] == $dpd_courier_tax_class_id) { ?>
                        <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                        <?php } ?>
                        <?php } ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_cod_status; ?></label>
                    <div class="col-sm-10">
                      <select name="dpd_courier_same_cod_status" id="input-cod-status" class="form-control">
                        <?php if ($dpd_courier_same_cod_status) { ?>
                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                        <option value="0"><?php echo $text_disabled; ?></option>
                        <?php } else { ?>
                        <option value="1"><?php echo $text_enabled; ?></option>
                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_monday; ?> <?php echo $help_day; ?></label>

                    <div class="col-sm-5">
                      <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_from; ?></span>
                        <input name="dpd_courier_same_mon_pickup[1][from]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_mon_pickup[1]['from']; ?>">
                        
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_until; ?></span>
                        <input name="dpd_courier_same_mon_pickup[1][until]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_mon_pickup[1]['until']; ?>">
                      </div>
                    </div>

                    <div class="col-sm-5">
                      <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_from; ?></span>
                        <input name="dpd_courier_same_mon_pickup[2][from]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_mon_pickup[2]['from']; ?>">
                        
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_until; ?></span>
                        <input name="dpd_courier_same_mon_pickup[2][until]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_mon_pickup[2]['until']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_tuesday; ?> <?php echo $help_day; ?></label>

                    <div class="col-sm-5">
                      <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_from; ?></span>
                        <input name="dpd_courier_same_tue_pickup[1][from]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_tue_pickup[1]['from']; ?>">
                        
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_until; ?></span>
                        <input name="dpd_courier_same_tue_pickup[1][until]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_tue_pickup[1]['until']; ?>">
                      </div>
                    </div>

                    <div class="col-sm-5">
                      <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_from; ?></span>
                        <input name="dpd_courier_same_tue_pickup[2][from]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_tue_pickup[2]['from']; ?>">
                        
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_until; ?></span>
                        <input name="dpd_courier_same_tue_pickup[2][until]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_tue_pickup[2]['until']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_wednesday; ?> <?php echo $help_day; ?></label>

                    <div class="col-sm-5">
                      <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_from; ?></span>
                        <input name="dpd_courier_same_wed_pickup[1][from]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_wed_pickup[1]['from']; ?>">
                        
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_until; ?></span>
                        <input name="dpd_courier_same_wed_pickup[1][until]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_wed_pickup[1]['until']; ?>">
                      </div>
                    </div>

                    <div class="col-sm-5">
                      <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_from; ?></span>
                        <input name="dpd_courier_same_wed_pickup[2][from]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_wed_pickup[2]['from']; ?>">
                        
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_until; ?></span>
                        <input name="dpd_courier_same_wed_pickup[2][until]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_wed_pickup[2]['until']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_thursday; ?> <?php echo $help_day; ?></label>

                    <div class="col-sm-5">
                      <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_from; ?></span>
                        <input name="dpd_courier_same_thu_pickup[1][from]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_thu_pickup[1]['from']; ?>">
                        
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_until; ?></span>
                        <input name="dpd_courier_same_thu_pickup[1][until]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_thu_pickup[1]['until']; ?>">
                      </div>
                    </div>

                    <div class="col-sm-5">
                      <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_from; ?></span>
                        <input name="dpd_courier_same_thu_pickup[2][from]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_thu_pickup[2]['from']; ?>">
                        
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_until; ?></span>
                        <input name="dpd_courier_same_thu_pickup[2][until]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_thu_pickup[2]['until']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_friday; ?> <?php echo $help_day; ?></label>

                    <div class="col-sm-5">
                      <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_from; ?></span>
                        <input name="dpd_courier_same_fri_pickup[1][from]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_fri_pickup[1]['from']; ?>">
                        
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_until; ?></span>
                        <input name="dpd_courier_same_fri_pickup[1][until]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_fri_pickup[1]['until']; ?>">
                      </div>
                    </div>

                    <div class="col-sm-5">
                      <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_from; ?></span>
                        <input name="dpd_courier_same_fri_pickup[2][from]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_fri_pickup[2]['from']; ?>">
                        
                        <span class="input-group-addon" id="basic-addon1"><?php echo $text_until; ?></span>
                        <input name="dpd_courier_same_fri_pickup[2][until]" type="text" class="form-control time" value="<?php echo $dpd_courier_same_fri_pickup[2]['until']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $entry_status_per_day; ?></label>
                    <div class="col-sm-10">
                      <div class="well well-sm" style="height: 170px; overflow: auto;">
                        <?php foreach ($weekdays as $key => $day) {  ?>
                        <div class="checkbox">
                          <label>
                            <?php if (isset($status_per_day) && in_array($key, $status_per_day)) { ?>
                              <input type="checkbox" name="dpd_courier_same_weekdays[]" value="<?php echo $key; ?>" checked />
                              <?php echo str_replace(':', '', $day); ?>
                            <?php } else { ?>
                              <input type="checkbox" name="dpd_courier_same_weekdays[]" value="<?php echo $key; ?>" />
                              <?php echo str_replace(':', '', $day); ?>
                            <?php } ?>
                          </label>
                        </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                    <div class="col-sm-10">
                      <select name="dpd_courier_same_status" id="input-status" class="form-control">
                        <?php if ($dpd_courier_same_status) { ?>
                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                        <option value="0"><?php echo $text_disabled; ?></option>
                        <?php } else { ?>
                        <option value="1"><?php echo $text_enabled; ?></option>
                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="dpd_courier_same_sort_order" value="<?php echo $dpd_courier_same_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                    </div>
                  </div>
                </div>

                <?php foreach ($geo_zones as $geo_zone) { ?>
                  <div class="tab-pane" id="tab-geo-zone<?php echo $geo_zone['geo_zone_id']; ?>">
                    <?php if($price_calculation_method == 'weight') { ?>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-rate<?php echo $geo_zone['geo_zone_id']; ?>"><span data-toggle="tooltip" title="<?php echo $help_rate; ?>"><?php echo $entry_rate; ?></span></label>
                        <div class="col-sm-10">
                          <textarea name="dpd_courier_same_<?php echo $geo_zone['geo_zone_id']; ?>_rate" rows="5" placeholder="<?php echo $entry_rate; ?>" id="input-rate<?php echo $geo_zone['geo_zone_id']; ?>" class="form-control"><?php echo ${'dpd_courier_same_' . $geo_zone['geo_zone_id'] . '_rate'}; ?></textarea>
                        </div>
                      </div>
                    <?php } else { ?>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-rate<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $entry_price; ?></span></label>
                        <div class="col-sm-10">
                          <input name="dpd_courier_same_<?php echo $geo_zone['geo_zone_id']; ?>_price" id="input-rate<?php echo $geo_zone['geo_zone_id']; ?>" class="form-control" value="<?php echo ${'dpd_courier_same_' . $geo_zone['geo_zone_id'] . '_price'}; ?>" />
                        </div>
                      </div>
                    <?php } ?>

                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="input-free-shipping<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $entry_free_shipping; ?></label>
                      <div class="col-sm-10">
                        <input type="text" name="dpd_courier_same_<?php echo $geo_zone['geo_zone_id']; ?>_free_shipping_from" placeholder="<?php echo $entry_free_shipping; ?>" id="input-free-shipping<?php echo $geo_zone['geo_zone_id']; ?>" value="<?php echo ${'dpd_courier_same_' . $geo_zone['geo_zone_id'] . '_free_shipping_from'}; ?>" class="form-control" />
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="input-status<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $entry_status; ?></label>
                      <div class="col-sm-10">
                        <select name="dpd_courier_same_<?php echo $geo_zone['geo_zone_id']; ?>_status" id="input-status<?php echo $geo_zone['geo_zone_id']; ?>" class="form-control">
                          <?php if (${'dpd_courier_same_' . $geo_zone['geo_zone_id'] . '_status'}) { ?>
                          <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                          <option value="0"><?php echo $text_disabled; ?></option>
                          <?php } else { ?>
                          <option value="1"><?php echo $text_enabled; ?></option>
                          <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  $('.time').datetimepicker({
    format: 'HH:mm',
    pickDate: false
  });
</script>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">

<?php echo $footer; ?> 