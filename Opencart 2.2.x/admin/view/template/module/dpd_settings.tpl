<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  
    <div class="page-header">
      <div class="container-fluid">
        <div class="pull-right">
          <button type="submit" id="form-submit" class="btn btn-primary" data-toggle="tooltip" title="<?php echo $button_save; ?>" ><i class="fa fa-save"></i></button>

          <a href="<?php echo $cancel; ?>"  class="btn btn-default"><i class="fa fa-reply"></i></a></div>
        <h1><?php echo $heading_title; ?></h1>
        <ul class="breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
          <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
    </div>
    <div class="container-fluid">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
        </div>
        <div class="panel-body">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab-company" data-toggle="tab"><?php echo $tab_company; ?></a></li>
            <li><a href="#tab-parcel-configuration" data-toggle="tab"><?php echo $tab_parcel_configuration; ?></a></li>
            <li><a href="#tab-manifest" data-toggle="tab">Manifest</a></li>
            <li><a href="#tab-collection-request" data-toggle="tab"><?php echo $tab_collection_request; ?></a></li>
          </ul>

          <form id="form-submit" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
            <div class="tab-content">
              <div class="tab-pane active" id="tab-general">
                <div class="form-group required">
                  <label class="col-sm-2 control-label" for="api_username"><?php echo $entry_dpd_setting_api_username; ?></label>
                  <div class="col-sm-10">
                    <input type="text" name="dpd_setting_api_username" id="api_username" class="form-control" value="<?php echo $dpd_setting_api_username; ?>" />
                    <?php if ($error_dpd_setting_api_username) { ?>
                      <span class="text-danger"><?php echo $error_dpd_setting_api_username; ?></span>
                    <?php } ?>
                  </div>
                </div>

                <div class="form-group required">
                  <label class="col-sm-2 control-label" for="api_password"><?php echo $entry_dpd_setting_api_password; ?></label>
                  <div class="col-sm-10">
                    <input type="password" name="dpd_setting_api_password" id="api_password" class="form-control" value="<?php echo $dpd_setting_api_password; ?>" />
                    <?php if ($error_dpd_setting_api_password) { ?>
                      <span class="text-danger"><?php echo $error_dpd_setting_api_password; ?></span>
                    <?php } ?>
                  </div>
                </div>

                <div class="form-group required">
                  <label class="col-sm-2 control-label" for="api_url"><?php echo $entry_dpd_setting_api_url; ?></label>
                  <div class="col-sm-10">
                    <select name="dpd_setting_api_url" id="dpd_setting_api_url" class="form-control">
                      <option value="lt" <?php if ($dpd_setting_api_url == 'lt') { echo 'selected'; } ?>>Lithuania</option>
                      <option value="lv" <?php if ($dpd_setting_api_url == 'lv') { echo 'selected'; } ?>>Latvia</option>
                      <option value="ee" <?php if ($dpd_setting_api_url == 'ee') { echo 'selected'; } ?>>Estonia</option>
                    </select>

                    <?php if ($error_dpd_setting_api_url) { ?>
                      <span class="text-danger"><?php echo $error_dpd_setting_api_url; ?></span>
                    <?php } ?>

                    <!-- <input type="text" name="dpd_setting_api_url" id="api_url" class="form-control" value="<?php echo $dpd_setting_api_url; ?>" /> -->
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label" for="google_map_api_key"><?php echo $entry_dpd_setting_google_map_api_key; ?></label>
                  <div class="col-sm-10">
                    <input name="dpd_setting_google_map_api_key" id="google_map_api_key" class="form-control"  value="<?php echo $dpd_setting_google_map_api_key; ?>" />
                    <!-- <?php if ($error_dpd_setting_google_map_api_key) { ?>
                      <span class="text-danger"><?php echo $error_dpd_setting_google_map_api_key; ?></span>
                    <?php } ?> -->
                  </div>
                </div>
              </div>

              <div class="tab-pane" id="tab-company">
                <div class="alert alert-info"><?php echo $info_warehouses; ?></div>

                <div class="table-responsive">
                  <table id="warehouse" class="table table-striped table-bordered table-hover">
                    <thead>
                      <tr>
                        <td class="text-left required" style="min-width: 100px;"><?php echo $column_name; ?></td>
                        <td class="text-left required" style="min-width: 100px;"><?php echo $column_address; ?></td>
                        <td class="text-left required" style="min-width: 100px;"><?php echo $column_postcode; ?></td>
                        <td class="text-left required" style="min-width: 100px;"><?php echo $column_city; ?></td>
                        <td class="text-left required" style="min-width: 100px;"><?php echo $column_country; ?></td>
                        <td class="text-left required" style="min-width: 100px;"><?php echo $column_contact_person; ?></td>
                        <td class="text-left required" style="min-width: 100px;"><?php echo $column_phone; ?></td>
                        <td class="text-right" style="min-width: 100px;"></td>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $warehouse_row = 0; ?>
                      <?php if ($dpd_setting_warehouses) { ?>
                        <?php foreach ($dpd_setting_warehouses as $dpd_setting_warehouse) { ?>
                        <tr id="warehouse-row<?php echo $warehouse_row; ?>">
                          <td class="text-left">
                            <input type="text" name="dpd_setting_warehouse[<?php echo $warehouse_row; ?>][name]" value="<?php echo $dpd_setting_warehouse['name']; ?>" placeholder="<?php echo $entry_name; ?>" class="form-control" required="true" />
                          </td>
                          <td class="text-left">
                            <input type="text" name="dpd_setting_warehouse[<?php echo $warehouse_row; ?>][address]" value="<?php echo $dpd_setting_warehouse['address']; ?>" placeholder="<?php echo $entry_address; ?>" class="form-control" required="true" />
                          </td>
                          <td class="text-left">
                            <input type="number" name="dpd_setting_warehouse[<?php echo $warehouse_row; ?>][postcode]" value="<?php echo $dpd_setting_warehouse['postcode']; ?>" placeholder="<?php echo $entry_postcode; ?>" class="form-control" required="true" />
                          </td>
                          <td class="text-left">
                            <input type="text" name="dpd_setting_warehouse[<?php echo $warehouse_row; ?>][city]" value="<?php echo $dpd_setting_warehouse['city']; ?>" placeholder="<?php echo $entry_city; ?>" class="form-control" required="true" />
                          </td>
                          <td class="text-left">
                            <select name="dpd_setting_warehouse[<?php echo $warehouse_row; ?>][country_id]" class="form-control" required="true">';
                              <option value="0"><?php echo $text_select; ?></option>
                              <?php foreach ($countries as $country) { ?>
                                <option <?php if ($dpd_setting_warehouse['country_id'] == $country['country_id']) { echo 'selected'; } ?> value="<?php echo $country['country_id']; ?>"><?php echo addslashes($country['name']); ?></option>
                              <?php } ?>   
                            </select>
                          </td>
                          <td class="text-left">
                            <input type="text" name="dpd_setting_warehouse[<?php echo $warehouse_row; ?>][contact_person_name]" value="<?php echo $dpd_setting_warehouse['contact_person_name']; ?>" placeholder="<?php echo $entry_contact_person_name; ?>" class="form-control" required="true" />
                          </td>
                          <td class="text-left">
                            <input type="text" name="dpd_setting_warehouse[<?php echo $warehouse_row; ?>][phone]" value="<?php echo $dpd_setting_warehouse['phone']; ?>" placeholder="<?php echo $entry_warehouse_phone; ?>" class="form-control" required="true" />
                          </td>
                          <td class="text-right">
                            <button type="button" onclick="$('#warehouse-row<?php echo $warehouse_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                          </td>
                        </tr>
                        <?php $warehouse_row++; ?>
                        <?php } ?>
                      <?php } ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="7"></td>
                        <td class="text-right"><button type="button" onclick="addWarehouse();" data-toggle="tooltip" title="<?php echo $button_warehouse_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>

              <div class="tab-pane" id="tab-parcel-configuration">
                <div class="form-group required">
                  <label class="col-sm-2 control-label" for="label_size"><?php echo $entry_dpd_label_size; ?></label>
                  <div class="col-sm-10">
                    <select name="dpd_setting_label_size" id="label_size" class="form-control">
                      <?php if($dpd_setting_label_size == 'A4') { ?>
                        <option value="A4" selected>A4</option>
                        <option value="A6">A6</option>
                      <?php } else if ($dpd_setting_label_size == 'A6') { ?>
                        <option value="A4">A4</option>
                        <option value="A6" selected>A6</option>
                      <?php } else { ?>
                        <option value="A4">A4</option>
                        <option value="A6">A6</option>
                      <?php } ?>
                    </select>
                    <?php if ($error_dpd_setting_label_size) { ?>
                      <span class="text-danger"><?php echo $error_dpd_setting_label_size; ?></span>
                    <?php } ?>
                  </div>
                </div>

                <div class="form-group required">
                  <label class="col-sm-2 control-label" for="parcel_distribution"><?php echo $entry_dpd_parcel_distribution; ?></label>
                  <div class="col-sm-10">
                    <select name="dpd_setting_parcel_distribution" id="parcel_distribution" class="form-control">
                      <?php if($dpd_setting_parcel_distribution == 1) { ?>
                        <option value="1" selected><?php echo $text_one_shipment; ?></option>
                        <option value="2"><?php echo $text_separate_shipment; ?></option>
                        <option value="3"><?php echo $text_separate_quantity_shipment; ?></option>
                      <?php } else if ($dpd_setting_parcel_distribution == 2) { ?>
                        <option value="1"><?php echo $text_one_shipment; ?></option>
                        <option value="2" selected><?php echo $text_separate_shipment; ?></option>
                        <option value="3"><?php echo $text_separate_quantity_shipment; ?></option>
                      <?php } else if ($dpd_setting_parcel_distribution == 3) { ?>
                        <option value="1"><?php echo $text_one_shipment; ?></option>
                        <option value="2"><?php echo $text_separate_shipment; ?></option>
                        <option value="3" selected><?php echo $text_separate_quantity_shipment; ?></option>
                      <?php } else { ?>
                        <option value="1"><?php echo $text_one_shipment; ?></option>
                        <option value="2"><?php echo $text_separate_shipment; ?></option>
                        <option value="3" selected><?php echo $text_separate_quantity_shipment; ?></option>
                      <?php } ?>
                    </select>

                    <?php if ($error_dpd_setting_parcel_distribution) { ?>
                      <span class="text-danger"><?php echo $error_dpd_setting_parcel_distribution; ?></span>
                    <?php } ?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label" for="return_service"><?php echo $entry_dpd_return_services; ?></label>
                  <div class="col-sm-10">
                    <select name="dpd_setting_return_service" id="return_service" class="form-control">
                      <?php if($dpd_setting_return_service == '1') { ?>
                        <option value="1" selected><?php echo $text_enabled; ?></option>
                        <option value="0"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                        <option value="1"><?php echo $text_enabled; ?></option>
                        <option value="0" selected><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label" for="rod_service"><?php echo $entry_dpd_rod_services; ?></label>
                  <div class="col-sm-10">
                    <select name="dpd_setting_rod_service" id="rod_service" class="form-control">
                      <?php if($dpd_setting_rod_service == '1') { ?>
                        <option value="1" selected><?php echo $text_enabled; ?></option>
                        <option value="0"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                        <option value="1"><?php echo $text_enabled; ?></option>
                        <option value="0" selected><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label" for="dpd_setting_price_calculation"><?php echo $entry_dpd_setting_price_calculation; ?></label>
                  <div class="col-sm-10">
                    <select name="dpd_setting_price_calculation" id="dpd_setting_price_calculation" class="form-control">
                      <option value="item" <?php if ($dpd_setting_price_calculation == 'item') { echo 'selected'; } ?>><?php echo $text_per_item; ?></option>
                      <option value="order" <?php if ($dpd_setting_price_calculation == 'order') { echo 'selected'; } ?>><?php echo $text_per_order; ?></option>
                      <option value="weight" <?php if ($dpd_setting_price_calculation == 'weight') { echo 'selected'; } ?>><?php echo $text_per_weight; ?></option>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label" for="dpd_setting_price_calculation_parcels"><?php echo $entry_dpd_setting_price_calculation_parcels; ?></label>
                  <div class="col-sm-10">
                    <select name="dpd_setting_price_calculation_parcels" id="dpd_setting_price_calculation_parcels" class="form-control">
                      <option value="order" <?php if ($dpd_setting_price_calculation_parcels == 'order') { echo 'selected'; } ?>><?php echo $text_per_order; ?></option>
                      <option value="weight" <?php if ($dpd_setting_price_calculation_parcels == 'weight') { echo 'selected'; } ?>><?php echo $text_per_weight; ?></option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="tab-pane" id="tab-manifest">
                <table class="table table-stripe">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo $text_current_manifest_day; ?></th>
                      <th class="text-right"><?php echo $text_action; ?></th>
                    </tr>
                  </thead>
                  <?php
                    if ($manifest_list) {
                      foreach ($manifest_list as $manifest) { ?>
                      <tr>
                        <td><?php echo $manifest['filename']; ?></td>
                        <td><?php echo $manifest['date']; ?></td>
                        <td class="text-right"><a href="<?php echo $manifest['preview']; ?>" target="_blank" class="btn btn-primary">Preview</a></td>
                      </tr>
                  <?php
                      }
                    }
                  ?>
                </table>
              </div>

              <div class="tab-pane" id="tab-collection-request">
                <!-- <form id="collectionRequestForm" class="form-controll" method="POST" novalidate="novalidate"> -->
                  <label class="pickup_title"><h3><?php echo $entry_pickup_title; ?></h3></label>
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="pickup_name"><?php echo $entry_pickup_name; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="collectionRequest[pickup_name]" id="pickup_name" class="form-control" maxlength="140" required />
                    </div>
                  </div>

                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="pickup_address"><?php echo $entry_pickup_address; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="collectionRequest[pickup_address]" id="pickup_address" class="form-control" maxlength="35" required />
                    </div>
                  </div>

                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="pickup_postcode"><?php echo $entry_pickup_postcode; ?></label>
                    <div class="col-sm-10">
                      <input type="number" name="collectionRequest[pickup_postcode]" id="pickup_postcode" class="form-control" maxlength="8" required />
                    </div>
                  </div>

                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="pickup_city"><?php echo $entry_pickup_city; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="collectionRequest[pickup_city]" id="pickup_city" class="form-control" maxlength="25" required />
                    </div>
                  </div>

                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="pickup_country"><?php echo $entry_pickup_country; ?></label>
                    <div class="col-sm-10">
                      <select name="collectionRequest[pickup_country]" class="form-control" id="pickup_country" required>
                        <!-- <option value="0"><?php echo $text_select; ?></option> -->
                        <?php foreach ($countries as $country) { ?>
                          <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="pickup_contact"><?php echo $entry_pickup_contact; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="collectionRequest[pickup_contact]" id="pickup_contact" class="form-control" maxlength="20" />
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="pickup_contact_email"><?php echo $entry_pickup_contact_email; ?></label>
                    <div class="col-sm-10">
                      <input type="email" name="collectionRequest[pickup_contact_email]" id="pickup_contact_email" class="form-control" maxlength="30" />
                    </div>
                  </div>


                  <!-- Recipient -->
                  <label style="margin-top: 30px;"><h3><?php echo $entry_pickup_recipient_title; ?></h3></label>
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="recipient_name"><?php echo $entry_pickup_recipient_name; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="collectionRequest[recipient_name]" id="recipient_name" class="form-control" maxlength="70"  />
                    </div>
                  </div>

                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="recipient_pickup_address"><?php echo $entry_pickup_recipient_address; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="collectionRequest[recipient_pickup_address]" id="recipient_pickup_address" class="form-control" maxlength="35" required />
                    </div>
                  </div>

                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="recipient_pickup_postcode"><?php echo $entry_pickup_recipient_postcode; ?></label>
                    <div class="col-sm-10">
                      <input type="number" name="collectionRequest[recipient_pickup_postcode]" id="recipient_pickup_postcode" class="form-control" maxlength="8" required />
                    </div>
                  </div>

                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="recipient_pickup_city"><?php echo $entry_pickup_recipient_city; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="collectionRequest[recipient_pickup_city]" id="recipient_pickup_city" class="form-control" />
                    </div>
                  </div>

                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="recipient_pickup_country"><?php echo $entry_pickup_recipient_country; ?></label>
                    <div class="col-sm-10">
                      <select name="collectionRequest[recipient_pickup_country]" class="form-control" id="recipient_pickup_country" required>
                        <option value=""><?php echo $text_select; ?></option>
                        <option value="LT">Lithuania</option>
                        <option value="LV">Latvia</option>
                        <option value="EE">Estonia</option>  
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="recipient_pickup_contact"><?php echo $entry_pickup_contact; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="collectionRequest[recipient_pickup_contact]" id="recipient_pickup_contact" class="form-control" maxlength="20" />
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="recipient_pickup_contact_email"><?php echo $entry_pickup_contact_email; ?></label>
                    <div class="col-sm-10">
                      <input type="email" name="collectionRequest[recipient_pickup_contact_email]" id="recipient_pickup_contact_email" class="form-control" maxlength="30" />
                    </div>
                  </div>

                  <!-- Information about parcel -->
                  <label style="margin-top: 30px;"><h3><?php echo $entry_parcels_title; ?></h3></label>

                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="pickup_parcels"><?php echo $entry_pickup_parcels_information; ?></label>
                    <div class="col-sm-3">
                      <input type="number" name="collectionRequest[parcels]" placeholder="<?php echo $entry_placeholder_parcels; ?>" id="pickup_parcels" class="form-control" maxlength="30" />
                    </div>

                    <div class="col-sm-3">
                      <input type="number" name="collectionRequest[pallets]" placeholder="<?php echo $entry_placeholder_pallets; ?>" id="pickup_pallets" class="form-control" maxlength="30" />
                    </div>

                    <div class="col-sm-3">
                      <input type="number" name="collectionRequest[weight]" placeholder="<?php echo $entry_placeholder_weight; ?>" id="pickup_weight" class="form-control" maxlength="30" />
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="parcels_additional_information"><?php echo $entry_pickup_parcels_additional_information; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="collectionRequest[pickup_parcels_additional_information]" id="parcels_additional_information" class="form-control" maxlength="30" />
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="parcels_pickupdate"><?php echo $entry_pickup_parcels_date; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="collectionRequest[pickup_parcels_date]" id="parcels_pickupdate" class="form-control date" value="<?php echo date('Y-m-d', strtotime('+1 day')) ?>" />
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-2">
                      <button id="requestCollection" data-loading-text="Loading..." type="button" class="btn btn-block btn-primary"><?php echo $entry_request; ?></button>
                    </div>
                  </div>

                <script>
                  $("#requestCollection").click(function(e) {
                    e.preventDefault();

                    var form = $("form");
                    var data = form.serializeArray();

                    $.ajax({
                      type: "POST",
                      url: '<?php echo str_replace("&amp;", "&", $collectionRequest); ?>',
                      data: data, // serializes the form's elements.
                      beforeSend: function() {
                        form.find('.text-danger').remove();
                        $('#requestCollection').button('loading');
                      },
                      success: function(data) {
                        var obj = jQuery.parseJSON(data);

                        $('.alert').remove();

                        // Validate fields
                        if (obj.error) {
                          if (obj.error.pickup_name) {
                            $('input#pickup_name').after('<span class="text-danger">' + obj.error.pickup_name + '</span>').fadeIn();
                          } else {
                            $('input#pickup_name').find('.text-danger').remove();
                          }

                          if (obj.error.pickup_address) {
                            $('input#pickup_address').after('<span class="text-danger">' + obj.error.pickup_address + '</span>').fadeIn();
                          } else {
                            $('input#pickup_address').find('.text-danger').remove();
                          }

                          if (obj.error.pickup_postcode) {
                            $('input#pickup_postcode').after('<span class="text-danger">' + obj.error.pickup_postcode + '</span>').fadeIn();
                          } else {
                            $('input#pickup_postcode').find('.text-danger').remove();
                          }

                          if (obj.error.pickup_city) {
                            $('input#pickup_city').after('<span class="text-danger">' + obj.error.pickup_city + '</span>').fadeIn();
                          } else {
                            $('input#pickup_city').find('.text-danger').remove();
                          }

                          if (obj.error.pickup_country) {
                            $('select#pickup_country').after('<span class="text-danger">' + obj.error.pickup_country + '</span>').fadeIn();
                          } else {
                            $('select#pickup_country').find('.text-danger').remove();
                          }

                          if (obj.error.recipient_name) {
                            $('input#recipient_name').after('<span class="text-danger">' + obj.error.recipient_name + '</span>').fadeIn();
                          } else {
                            $('input#recipient_name').find('.text-danger').remove();
                          }

                          if (obj.error.recipient_pickup_address) {
                            $('input#recipient_pickup_address').after('<span class="text-danger">' + obj.error.recipient_pickup_address + '</span>').fadeIn();
                          } else {
                            $('input#recipient_pickup_address').find('.text-danger').remove();
                          }

                          if (obj.error.recipient_pickup_postcode) {
                            $('input#recipient_pickup_postcode').after('<span class="text-danger">' + obj.error.recipient_pickup_postcode + '</span>').fadeIn();
                          } else {
                            $('input#recipient_pickup_postcode').find('.text-danger').remove();
                          }

                          if (obj.error.recipient_pickup_city) {
                            $('input#recipient_pickup_city').after('<span class="text-danger">' + obj.error.recipient_pickup_city + '</span>').fadeIn();
                          } else {
                            $('input#recipient_pickup_city').find('.text-danger').remove();
                          }

                          if (obj.error.recipient_pickup_country) {
                            $('select#recipient_pickup_country').after('<span class="text-danger">' + obj.error.recipient_pickup_country + '</span>').fadeIn();
                          } else {
                            $('select#recipient_pickup_country').find('.text-danger').remove();
                          }

                          if (obj.error.weight) {
                            $('input#pickup_weight').after('<span class="text-danger">' + obj.error.weight + '</span>').fadeIn();
                          } else {
                            $('input#pickup_weight').find('.text-danger').remove();
                          }

                          if (obj.error.parcels) {
                            $('input#pickup_parcels').after('<span class="text-danger">' + obj.error.parcels + '</span>').fadeIn();
                          } else {
                            $('input#pickup_parcels').find('.text-danger').remove();
                          }

                          
                        } else {
                          if(obj.response.error) {
                            $('label.pickup_title').before('<div class="alert alert-danger">' + obj.response.error + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                          } else {
                            $('label.pickup_title').before('<div class="alert alert-success">' + obj.response.success + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                          }

                          $('html, body').animate({
                            scrollTop: $(".alert").offset().top
                          }, 2000);
                        }

                        $('#requestCollection').button('reset');
                      }
                    });
                  });
                </script>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  
</div>

<script type="text/javascript"><!--
  var warehouse_row = <?php echo $warehouse_row; ?>;

  function addWarehouse() {
    html  = '<tr id="warehouse-row' + warehouse_row + '">';
    html += '  <td class="text-left"><input type="text" name="dpd_setting_warehouse[' + warehouse_row + '][name]" value="" placeholder="<?php echo $entry_address; ?>" class="form-control" required="true" /></td>';
    html += '  <td class="text-left"><input type="text" name="dpd_setting_warehouse[' + warehouse_row + '][address]" value="" placeholder="<?php echo $entry_address; ?>" class="form-control" required="true" /></td>';
    html += '  <td class="text-left"><input type="number" name="dpd_setting_warehouse[' + warehouse_row + '][postcode]" value="" placeholder="<?php echo $entry_postcode; ?>" class="form-control" required="true" /></td>';
    html += '  <td class="text-left"><input type="text" name="dpd_setting_warehouse[' + warehouse_row + '][city]" value="" placeholder="<?php echo $entry_city; ?>" class="form-control" required="true" /></td>';
    html += '  <td class="text-left"><select name="dpd_setting_warehouse[' + warehouse_row + '][country_id]" class="form-control" required="true">';
    html += '  <option value="0"><?php echo $text_select; ?></option>';
    <?php foreach ($countries as $country) { ?>
      html += '<option value="<?php echo $country['country_id']; ?>"><?php echo addslashes($country['name']); ?></option>';
    <?php } ?>   
    html += '  </select></td>';
    html += '  <td class="text-left"><input type="text" name="dpd_setting_warehouse[' + warehouse_row + '][contact_person_name]" value="" placeholder="<?php echo $entry_contact_person_name; ?>" class="form-control" required="true" /></td>';
    html += '  <td class="text-left"><input type="text" name="dpd_setting_warehouse[' + warehouse_row + '][phone]" value="" placeholder="<?php echo $entry_warehouse_phone; ?>" class="form-control" required="true" /></td>';
    html += '  <td class="text-right"><button type="button" onclick="$(\'#warehouse-row' + warehouse_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
    html += '</tr>';
    
    $('#warehouse tbody').append(html);
    
    warehouse_row++;
  }
</script>

<script type="text/javascript"><!--
  $(document).ready(function() {
    $('.date').datetimepicker({
      format: 'YYYY-MM-DD',
      pickDate: true,
      pickTime: false,
      minDate: new Date(),
      disabledDates: [new Date()],
      daysOfWeekDisabled: [0, 6],
    });
  });
</script>
<?php echo $footer; ?>