<form id="dpd-form" class="form-horizontal">
  <div class="form-group">
    <label for="warehouse" class="col-sm-4 control-label"><?php echo $entry_select_warehouse; ?></label>
    <div class="col-sm-8">
      <select name="warehouse_id" id="warehouse" class="form-control">
        <?php foreach ($warehouses as $key => $warehouse) { ?>
        <option value="<?php echo $key; ?>"><?php echo $warehouse['name']; ?></option>
        <?php } ?>
      </select>
    </div>
  </div> 

  <input type="hidden" name="order_id" value="<?php echo $order_id; ?>" />
</form>

<button id="dpd_submit" class="btn btn-block btn-info"><?php echo $button_request_dpd; ?></button>


<script type="text/javascript">
  $("#dpd_submit").on("click", function() {
    $.ajax({
      url: '<?php echo str_replace("&amp;", "&", $action); ?>&modal_ajax=1',
      type: 'post',
      data: $('#dpd-form').serialize(),
      dataType: 'json',
      beforeSend: function() {
        $('#dpd_submit').button('loading');
      },
      complete: function() {
        $('#dpd_submit').button('reset');
      },
      success: function(json) {
        $('.alert').remove();

        if (json['error']) {
          $('#dpd-form').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }

        if (json['success']) {
          $('#dpd-form').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        //alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  });
</script>